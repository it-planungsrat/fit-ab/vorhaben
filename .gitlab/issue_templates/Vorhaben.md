## Ersteller


## Erstellungsdatum


## Welcher IST-Zustand stellt aktuell welche/s Problem/e dar (siehe Steckbriefvorlage)?


## Wer ist von dem Problem betroffen (siehe Steckbriefvorlage)?


## Warum tritt das Problem auf (siehe Steckbriefvorlage)?

## Welches Ergebnis soll im Vorhaben erarbeitet werden? (siehe Steckbriefvorlage)?


## Welche Bestandteile muss das Ergebnis umfassen (siehe Steckbriefvorlage)?


## Relevante Links und Bemerkungen

- Link zum aktuellen Steckbrief:
- Link zur Umsetzungsplanung:
- Weitere Links und Bemerkungen:

## Eckdaten Umsetzungsplanung (siehe Vorlage Umsetzungsplanung)


- Federführung: 
- Bearbeitung/ Umsetzung durch:
- Aufwand/ Umsetzungsdauer:
- Start der Umsetzung: 

## Meilensteine



